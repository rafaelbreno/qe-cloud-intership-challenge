# Basic Message Insert Page
## Tools
- Database:
    - Postgresql and Pgadmin 4
- Python frameworks
    - Flask
    - psycopg2
## Step-by-Step development
### Database
- After the installation you need to create a _database_ and a _table_, in this case named "quero" and "messages", using the following SQL:
    - ```sql
      CREATE DATABASE quero;
      CREATE TABLE messages (
          id      SERIAL PRIMARY KEY,
          message varchar(255) NOT NULL
      );
      ```
### Application
- If you execute `python app.py` and try to access the designated URL _"http://0.0.0.0:8000/"_, It won't work, that's the first issue to solve
    - First we need to create some _"environment variables"_, In my case I need to use the command `setx VAR VALUE` because I'm using Windows, we will need the following variables:
        - > setx DB_HOST 127.0.0.1
        - > setx DB_NAME quero
        - > setx DB_PASSWD yourPGAdminPassWord
        - > setx DB_USER postgres
    - But it isn't finished yet, if you execute it again the prompt will show you the same URL, now we need to fix a line on __"app.py"__
        - From:
            - ```py
              if __name__ == "__main__":
                app.run(debug=True, host="0.0.0.0", port=8000)
              ```
        - To:
            - ```py
              if __name__ == "__main__":
                app.run(debug=True, host=host, port=8000)
              ```
- Now in your prompt should show a functional URL and when you openm It will show you an error
    - _"NameError: name 'render_template' is not defined"_
    - That means the "render_template" wasn't imported, to fix it we need to go back to _"app.py"_ and modify the 1st line
    - From:
        - ```py
            from flask import Flask, request, jsonify
          ```
    - To:
        - ```py
            from flask import Flask, request, jsonify, render_template
          ```
- Back to the page, another error should be shown
    - _"psycopg2.OperationalError: fe_sendauth: no password supplied"_
    - That means that we haven't informed _psycopg2_ our database password, to solve it we will need to change 2 parts of our code
    - From:
        - ```py
          <...>
          def read():
            conn = psycopg2.connect(
                  host=host,
                  database=database,
                  user=user
              )
          <...>
          def save(message):
            conn = psycopg2.connect(
                    host=host,
                    database=database,
                    user=user
                )
          ```
      - To:
        - ```py
          <...>
          def read():
            conn = psycopg2.connect(
                  host=host,
                  database=database,
                  user=user,
                  password=password
              )
          <...>
          def save(message):
            conn = psycopg2.connect(
                    host=host,
                    database=database,
                    user=user,
                    password=password
                )
          ```
- Now the page should open, but if you try to send a _"message"_ to the database the page should show an error _"Method Not Allowed"_, now back to _"app.py"_, in the line `@app.route`, you can see that it have only 1 allowed method(_GET_), then we should allow _POST_ method too.
    - From:
        - ```py
            @app.route('/', methods=['GET'])
          ```
    - To:
        - ```py
          @app.route('/', methods=['GET', 'POST'])
          ```
- With It everything should works just fine!